/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 *
 * COPYRIGHT(c) 2016 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     
#include "stm32f3_discovery.h"
#include "stm32f3_discovery_accelerometer.h"
#include "stm32f3_discovery_gyroscope.h"
#include "six_axis_comp_filter.h"
#include "tim.h"

#define MAX_GAZ 100
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId mainTaskHandle;
osThreadId getImuDataTaskHandle;
osThreadId communicationTaskHandle;
osThreadId calcuatePIDTaskHandle;

/* USER CODE BEGIN Variables */
uint8_t wykonaj_pomiar_katow, policz_pid;

uint8_t silniki_zainicjowane = 0;

uint8_t gaz;

SixAxis filter;

float x_data, y_data;

float x_angle, y_angle;

float Kp_x,Kd_x;

float Kp_y,Kd_y;
/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartMainTask(void const * argument);
void StartGetImuDataTask(void const * argument);
void StartCommunicationTask(void const * argument);
void StartCalcuatePIDTask(void const * argument);

extern void MX_USB_DEVICE_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */
void ustawPWM(uint8_t nrSilnika, uint8_t procentWypelnienia);
/* USER CODE END FunctionPrototypes */

/* Hook prototypes */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
	/* USER CODE END RTOS_MUTEX */

	/* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
	/* USER CODE END RTOS_SEMAPHORES */

	/* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
	/* USER CODE END RTOS_TIMERS */

	/* Create the thread(s) */
	/* definition and creation of mainTask */
	osThreadDef(mainTask, StartMainTask, osPriorityNormal, 0, 128);
	mainTaskHandle = osThreadCreate(osThread(mainTask), NULL);

	/* definition and creation of getImuDataTask */
	osThreadDef(getImuDataTask, StartGetImuDataTask, osPriorityNormal, 0, 1024);
	getImuDataTaskHandle = osThreadCreate(osThread(getImuDataTask), NULL);

	/* definition and creation of communicationTask */
	osThreadDef(communicationTask, StartCommunicationTask, osPriorityLow, 0, 128);
	communicationTaskHandle = osThreadCreate(osThread(communicationTask), NULL);

	/* definition and creation of calcuatePIDTask */
	osThreadDef(calcuatePIDTask, StartCalcuatePIDTask, osPriorityAboveNormal, 0, 1024);
	calcuatePIDTaskHandle = osThreadCreate(osThread(calcuatePIDTask), NULL);

	/* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
	/* USER CODE END RTOS_THREADS */

	/* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
	/* USER CODE END RTOS_QUEUES */
}

/* StartMainTask function */
void StartMainTask(void const * argument)
{
	/* init code for USB_DEVICE */
	MX_USB_DEVICE_Init();
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_3);

	ustawPWM(2,100);
	osDelay(2500);
	ustawPWM(2,0);
	osDelay(4000);
	silniki_zainicjowane = 1;
	/* USER CODE BEGIN StartMainTask */
	/* Infinite loop */
	for(;;)
	{

	}
	/* USER CODE END StartMainTask */
}

/* StartGetImuDataTask function */
void StartGetImuDataTask(void const * argument)
{
	/* USER CODE BEGIN StartGetImuDataTask */
	int16_t acc_data[3];
	float gyro_data[3];

	wykonaj_pomiar_katow = 0;

	BSP_GYRO_Init();
	BSP_ACCELERO_Init();
	CompInit(&filter,0.0001,0.001);
	BSP_ACCELERO_GetXYZ(acc_data);
	CompStart(&filter);
	/* Infinite loop */
	for(;;)
	{
		if(wykonaj_pomiar_katow)
		{
			BSP_ACCELERO_GetXYZ(acc_data);
			BSP_GYRO_GetXYZ(gyro_data);
			CompAccelUpdate(&filter,(acc_data[0]/1670),(acc_data[1]/1670),(acc_data[2]/1670));
			CompGyroUpdate(&filter,(gyro_data[1]/1140)-2.15f,gyro_data[2]/1140,(gyro_data[2]/1140)+0.5f);
			CompUpdate(&filter);
			CompAnglesGet(&filter,&x_data,&y_data);
			x_data = CompRadiansToDegrees(x_data);
			y_data = CompRadiansToDegrees(y_data);
			if(x_data > 180)
				x_angle = x_data - 360;
			else
				x_angle = x_data;
			if(y_data > 180)
				y_angle = y_data - 360;
			else
				y_angle = y_data;
			wykonaj_pomiar_katow = 0;
		}
	}
	/* USER CODE END StartGetImuDataTask */
}

/* StartCommunicationTask function */
void StartCommunicationTask(void const * argument)
{
	/* USER CODE BEGIN StartCommunicationTask */
	/* Infinite loop */
	for(;;)
	{
		osDelay(1);
	}
	/* USER CODE END StartCommunicationTask */
}

/* StartCalcuatePIDTask function */
void StartCalcuatePIDTask(void const * argument)
{
	/* USER CODE BEGIN StartCalcuatePIDTask */
	int16_t pwm_x, pwm_y;
	int16_t uchyb_x, uchyb_y;
	int16_t poprzedni_x, poprzedni_y;
	policz_pid = 0;
	Kp_x = 1;
	Kd_x = 0.1;
	Kp_y = 1;
	Kd_y = 0.1;
	uchyb_x = 0 - x_angle;
	uchyb_y = 0 - y_angle;
	poprzedni_x = uchyb_x;
	poprzedni_y = uchyb_y;
	//while(!silniki_zainicjowane);
	/* Infinite loop */
	for(;;)
	{
		if(policz_pid)
		{
			uchyb_x = 0 - x_angle;
			uchyb_y = 0 - y_angle;
			pwm_x = Kp_x*uchyb_x+Kd_x*(uchyb_x - poprzedni_x);
			pwm_y = Kp_y*uchyb_y+Kd_y*(uchyb_y - poprzedni_y);
			poprzedni_x = uchyb_x;
			poprzedni_y = uchyb_y;
			if(pwm_x >= 0)
			{
				ustawPWM(1,gaz-gaz*(pwm_x/MAX_GAZ));
				ustawPWM(2,gaz);
			}
			else
			{
				pwm_x *= -1;
				ustawPWM(2,gaz-gaz*(pwm_x/MAX_GAZ));
				ustawPWM(1,gaz);
			}
			ustawPWM(3,gaz-gaz*(pwm_y/MAX_GAZ));
			policz_pid = 0;
		}
	}
	/* USER CODE END StartCalcuatePIDTask */
}

/* USER CODE BEGIN Application */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == htim6.Instance)
		wykonaj_pomiar_katow = 1;
	if(htim->Instance == htim7.Instance)
		policz_pid = 1;
}

void ustawPWM(uint8_t nrSilnika, uint8_t procentWypelnienia)
{
	if(procentWypelnienia > 100)
		procentWypelnienia = 100;
	switch(nrSilnika)
	{
	case 1:
		htim2.Instance->CCR2 = 100+procentWypelnienia;
		break;
	case 2:
		htim2.Instance->CCR3 = 100+procentWypelnienia;
		break;
	case 3:
		htim2.Instance->CCR4 = 100+procentWypelnienia;
		break;
	}
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
